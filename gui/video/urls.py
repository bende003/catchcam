from django.conf.urls import url
from django.urls import include, path

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^ajax/event/$', views.event, name='event'),
    url(r'^download/$', views.download, name='download'),
    url(r'^camera/last.jpg$', views.preview, name='preview'),
    #url(r'^get/(?P<idx>[0-9\-a-f]+)/$', views.send_zipfile, name='get'),
    path('get/<uuid:idx>/', views.send_zipfile, name='get'),
    url(r'^del/$', views.delete, name='event'),
    url(r'^shutdown/$', views.shutdown, name='shutdown'),
]
