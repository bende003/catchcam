import os
import glob
import tempfile
import zipfile
import mimetypes
import datetime
from django.http import HttpResponse, FileResponse, StreamingHttpResponse
from django.http import JsonResponse
from django.template import loader
from django.shortcuts import render
from django.utils import timezone
from django.views.decorators.cache import never_cache
from django.core.signals import request_finished
from wsgiref.util import FileWrapper
from .models import CameraState, CameraConfig, PreviewImages, EventQueue, ImageCollection
from logging import info, debug, warning, error, critical
from django.db.models import Model
import logging

logging.basicConfig(format='%(levelname)s %(asctime)s:[Catchcam gui] %(message)s', filename='/data/catchcam/CatchCam-gui.log',level=logging.DEBUG)
zippers = {}

def get_data_dict(camstate):
    return {"nbimg": 0,
             "setid": "No dataset",
             "diskspace": camstate.diskspace,
             "state": get_state_name(),
             "allow_shutdown": True
            }
def get_state():
    return CameraState.objects.all().first()

def get_state_name():
    state =  str(CameraState.objects.all().first())
    debug("State : %s" % state)
    return "start" if state == "STARTED" else "stop"

def get_last_img_collection():
    return ImageCollection.objects.all().filter(name = "INTER").order_by('-started_at').first()

def is_stopped():
    camstate = get_state()
    return camstate.state == 'STOPPED'

def is_started():
    camstate = get_state()
    return camstate.state == 'STARTED'
    
def start():
    event = EventQueue()
    event.eventName = "START"
    try:
        event.save()
        info("[start] Start event created")
    except Exception as e:
        error("Error %s" % str(e))

def stop():
    event = EventQueue()
    event.eventName = "STOP"
    try:
        event.save()
        info("[stop] Stop event created")
    except Exception as e:
        error("Error %s" % str(e))

def mk_download_title(img_collection):
    if img_collection.started_at is not None:
        started_at = img_collection.started_at.strftime("%Y-%B-%d %H:%M")
    else:
        started_at = "unknown" 

    if img_collection.name == "SNAP":
        mode = "[ snapshots ]"
    else:
        mode = ""
    nb_img = img_collection.nb_images
    name = "%s - %d number of images %s" % (started_at, nb_img, mode)
    data = {'id': img_collection.ID,  
            'name': name,
            'downloadable': img_collection.downloadable
            }

    return data

def index(request):
    camstate = get_state()
    context = {'state': get_state_name(),
               'free diskspace': camstate.diskspace}
    return render(request, 'index.html', context)


def download(request):
    l=[]
    img_collections  = ImageCollection.objects.all().filter(downloadable=True).order_by('-started_at')
    for col in img_collections:
        if col.nb_images > 0:
            l.append(mk_download_title(col))
    context = {"state_list": l}
    return render(request, 'download.html', context)


@never_cache
def event(request):
    event = request.GET.get('event', None)
    camstate = get_state()
    data = {"state": get_state_name()}
    allow_shutdown = True
    if event == "start": 
        start()
        data = {"state": "start",
                "started": str(datetime.datetime.utcnow()),
                "allow_shutdown": allow_shutdown
                }
    
    elif event == "stop": 
        stop() 
        data = {"state": "stop",
                "allow_shutdown": allow_shutdown
                }

    elif event == "snapshot":
        event = EventQueue()
        event.eventName = "TAKESNAP"
        event.save()
        data = {"state": get_state_name()}
            
    elif event == "status":
        col = get_last_img_collection()
        camstate = get_state()
        data = get_data_dict(camstate)
        if col:
            data["nbimg"] = col.nb_images
            data["setid"] = col.ID
        
    addr = request.META["REMOTE_ADDR"]
    debug("[event] Event %s called, returning %s to %s" % (event, str(data), addr))
    return JsonResponse(data)

@never_cache
def preview(request):
    src = PreviewImages.objects.last()
    if src:
        img = src.image
        content_len = len(img)
    else:
        img = None
        content_len = 0
    response = HttpResponse(img, content_type='image/jpeg')
    response['Content-Length'] = content_len
    debug("[preview] Sending preview image")
    return response
       
def delete(request):
    if request.method == "POST":
        ids = request.POST.getlist('download')
        for i in ids:
            instance = ImageCollection.objects.get(ID=i)
            if instance.downloadable:
                try:
                    fname = instance.filename
                    debug("Removing: %s" % fname)
                    os.remove(fname)
                except FileNotFoundError as e:
                    pass 
                instance.delete()
    return download(request)

def shutdown(request):
    os.system('sudo shutdown now &')
    return render(request, 'shutdown.html',{})

@never_cache
def send_zipfile(request, idx):
    try:
        img_col = ImageCollection.objects.get(ID=idx)
    except Exception as e:
        print(e)
        return HttpResponse("No such File")
    fp = img_col.filename
    fn = os.path.basename(fp)
    debug("[send_zipfile] Downloading file : %s" % fp)
    if os.path.exists(fp):
        logging.debug("Sending: %s" % fp)
        mime_type_guess = mimetypes.guess_type(fp)
        content_len = os.path.getsize(fp)
        zfile = open(fp, 'rb')
        logging.debug(str(zfile))
        response = FileResponse(zfile, content_type=mime_type_guess)
        logging.debug(str(response))
        response['Content-Disposition'] = "attachment; filename=%s" % fn
        response['Content-Length'] = content_len
        logging.debug(str(response))
        return response

    return HttpResponse("No such File")
