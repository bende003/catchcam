import uuid
import hashlib
from datetime import datetime
from django.db import models
# Create your models here.

class CameraConfig(models.Model):
    fieldname = models.CharField(max_length=255)
    value = models.CharField(max_length=255)

    def __str__(self):
        return "%s : %s" % (self.fieldname, self.value)

class PreviewImages(models.Model):
    image = models.BinaryField(null=True)
    timestamp = models.DateTimeField('preview timestamp', blank=True, default=None, null=True)

    def __str__(self):
        return "%s %s" % (str(self.timestamp), str(hashlib.sha256(self.image)))

class CameraState(models.Model):
    state = models.CharField(max_length=10)
    diskspace = models.IntegerField(blank=True, default=None, null=True)
    
    def __str__(self):
        return str(self.state)

class ImageCollection(models.Model):
    ID = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=5, choices=[('SNAP', 'SNAP'),('INTER', 'INTER')], default="INTER")
    started_at = models.DateTimeField('date started to record', blank=True, default=datetime.now, null=True)
    nb_images = models.IntegerField(default=0)
    filename = models.CharField(max_length=4096, blank=True, default="", null=True)
    downloadable = models.BooleanField(default=False)

class EventQueue(models.Model):
    ID = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    eventName = models.SlugField(max_length=32, choices=[("START", "START"), ("STOP","STOP"), ("TAKESNAP","TAKESNAP")])
    eventTime = models.DateTimeField(null=True, default=datetime.now, blank=True)

    eventData = models.TextField(blank=True, default="")
