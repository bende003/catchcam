var count = 30;
var frame = new Image();
frame.src = "/static/last.png";

var rec_off = new Image();
rec_off.src = "/static/recording_off.png";
var rec_on = new Image();
rec_on.src = "/static/recording_on.png";

function humanSize(kbytes) {
    if (kbytes) {
        thresh = 1024;
        if(Math.abs(kbytes) < thresh) {
            return kbytes + 'kiB';
        }

        var units =['MiB','GiB','TiB','PiB','EiB','ZiB','YiB'];
        var u = -1;
        do {
            kbytes /= thresh;
            ++u;
        } while(Math.abs(kbytes) >= thresh && u < units.length - 1);
        return kbytes.toFixed(1)+' '+units[u];
    }
    return "";
}

function updateFrame() {
    if (frame.complete) {
        document.getElementById("camera").src = frame.src;
        frame = new Image();
        frame.src = "/camera/last.jpg?at=" + new Date();
    }

}

function updateInfo(data) {
    document.getElementById("diskspace").innerHTML = "Free diskspace: " + humanSize(data.diskspace);
    document.getElementById("setid").innerHTML = "Current dataset id: " + data.setid;
    document.getElementById("nbimages").innerHTML = "Number of images in set: " + data.nbimg;
}

function updateState(state) {
    if (state == "start") {
        if (rec_on.complete) {
            document.getElementById("state").src = rec_on.src; 
        }
    } else {
        if (rec_off.complete) {
            document.getElementById("state").src = rec_off.src;
        }
    }
}


function Countdown(options) {
    var timer,
    instance = this,
    seconds = options.seconds || 10,
    updateStatus = options.onUpdateStatus || function () {},
    counterEnd = options.onCounterEnd || function () {};

    function decrementCounter() {
        updateStatus(seconds);
        if (seconds === 0) {
            counterEnd();
            instance.stop();
        }
        seconds--;
    };

    this.start = function () {
        clearInterval(timer);
        timer = 0;
        seconds = options.seconds;
        timer = setInterval(decrementCounter, 1000);
    };

    this.stop = function () {
        clearInterval(timer);
    };
}
