import os
import sys
import time
import io 
import picamera
from logging import debug, info, warning, error, critical

class Camera(object):
    def __init__(self, width, height, framerate):
        self.camera = picamera.PiCamera(resolution=(width, height))
        self.resolution = (width, height)
        self.camera.framerate = framerate
        time.sleep(2) # Meh
        # Now fix the values
#        self.camera.shutter_speed = self.camera.exposure_speed 
#        self.camera.exposure_mode = 'off'
#        g = self.camera.awb_gains
#        self.camera.awb_mode = 'off'
#        self.camera.awb_gains = g

    def read(self, resolution=None):
        img = io.BytesIO()
        info("Camera.read(): exposure speed: %d, shutter_speed: %d" % (self.camera.exposure_speed, self.camera.shutter_speed))
        try:
            if resolution is None:
                self.camera.capture(img, format='jpeg')
            else:
                self.camera.capture(img, format='jpeg',resize=resolution)
        except picamera.exc.PiCameraError as e :
            error("Camera Error while capturing  image: %s" % str(e))
            return None
        data = img.getvalue()
        img.close()
        return data

    def exif(self, exifkey, value):
        self.camera.exif_tags[exifkey] = str(value)

    def close(self):
        self.camera.close()

    def __get_exposurespeed(self):
        return self.camera.exposure_speed

    def __get_shutterspeed(self):
        return self.camera.shutter_speed

    def __set_shutterspeed(self, value):
        pass
 #       self.camera.shutter_speed = value

    shutterspeed = property(__get_shutterspeed, __set_shutterspeed)
    exposurespeed = property(__get_exposurespeed)
