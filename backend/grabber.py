import os
import io
import uuid
import time
import concurrent.futures
from threading import Lock
from datetime import datetime, timezone, date
import camera
import cv2
import numpy as np
import sqlalchemy
import tarfile 
from logging import debug, info, warning, error, critical

from sqlalchemy import create_engine, MetaData, Table, String, asc, desc
from sqlalchemy.orm import mapper, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql.expression import or_, and_, not_
engine = create_engine('postgresql://catchcam:CatchCam2017@localhost:5432/catchcam')
Session = sessionmaker(bind=engine)
session = Session()
metadata = MetaData(engine)
Base = declarative_base(metadata=metadata)

def get_free_diskspace():
    statvfs = os.statvfs('/data')
    return int((statvfs.f_frsize * statvfs.f_bavail) / 1024);

def mk_np_array(img):
    return np.asarray(bytearray(img))

class CameraConfig(Base):
    __tablename__ = 'video_cameraconfig'
    __table_args__ = {'autoload': True}

class PreviewImages(Base):
    __tablename__ = 'video_previewimages'
    __table_args__ = {'autoload': True}

class ImageCollection(Base):
    __tablename__ = 'video_imagecollection'
    __table_args__ = {'autoload': True}

class CameraState(Base):
    __tablename__ = 'video_camerastate'
    __table_args__ = {'autoload': True}

class EventQueue(Base):
    __tablename__ = 'video_eventqueue'
    __table_args__ = {'autoload': True}

tar_lock = Lock()

class DB(object):
    def __init__(self, session):
        self.__session = session
        self.__camstate = self.__session.query(CameraState).first()
        if not self.__camstate:
            self.__camstate = CameraState(state = "STOPPED", diskspace = get_free_diskspace())
            self.__session.add(self.__camstate)
            self.__session.commit()


    def __get_camstate(self):
        camstate = self.__session.query(CameraState).first()
        if not camstate:
            camstate = CameraState()
            self.__session.add(camstate)
            self.__session.commit()
        return camstate
            
    def update_diskspace(self):
        self.__camstate.diskspace = get_free_diskspace()
        self.__session.commit()
        
    def set_state(self, state):
        self.__camstate.state = state
        debug("camstate set to %s" % state)
        self.__session.commit()

    def has_state(self, state):
        return self.__camstate.state == state

    @property
    def interval(self):
        return int(self.__session.query(CameraConfig).filter_by(fieldname="interval").first().value)

    @property
    def cfg_img_dir(self):
        return self.__session.query(CameraConfig).filter_by(fieldname="img_basedir").first().value

    @property
    def max_tar_size(self):
        return int(self.__session.query(CameraConfig).filter_by(fieldname="max_files_in_tar").first().value)

    def img_directory(self, snapshot=False):
        camstate = self.currentstate(snapshot)
        if not camstate.img_directory:
            camstate.img_directory = self.cfg_img_dir
        if snapshot:
            d = os.path.join(camstate.img_directory, "snapshots")
        else:
            d = camstate.img_directory
        return d

    def store_preview_img(self, img):
        preview_obj = self.__session.query(PreviewImages).order_by(PreviewImages.id.desc()).first()
        if preview_obj is None:
            debug("CamDb:  Creating preview record in database!")
            preview_obj = PreviewImages()
            self.__session.add(preview_obj)
        preview_obj.image = bytearray(img)
        self.__session.commit()

    def new_image_collection(self):
        col = self.__session.query(ImageCollection).filter_by(name="INTER").filter_by(downloadable=False).order_by(desc('started_at')).first()
        if col:
            debug("new_image_collection: %s" % col.filename)
            if col.nb_images > 0:
                col.downloadable = True
            else:
                self.__session.delete(col)
        ts = datetime.utcnow() 
        set_id = ts.strftime("%Y%m%d%H%M%S")
        zfname = "CatchCam-%s.tar" % str(set_id)
        direct = self.cfg_img_dir
        fp = os.path.join(direct, zfname)
        col = ImageCollection(ID=str(uuid.uuid4()), name="INTER", downloadable=False, started_at=datetime.utcnow(), nb_images=0, filename=fp)
        self.__session.add(col)
        self.__session.commit()
        return col

    def get_last_image_collection(self):
        col = self.__session.query(ImageCollection).filter_by(name="INTER").filter_by(downloadable=False).order_by(desc('started_at')).first()
        if not col:
            col = self.new_image_collection()
        debug("get_last_image_collection: %s" % col.filename)
        return col

        
    def get_last_snapshot_collection(self):
        col = self.__session.query(ImageCollection).filter_by(name="SNAP").order_by(desc('started_at')).first()
        if not col or col.started_at.date() < datetime.utcnow().date():
            set_id = datetime.utcnow().date()
            zfname = "CatchCam-snapshots-%s.tar" % str(set_id)
            direct = os.path.join(self.cfg_img_dir, "snapshots")
            fp = os.path.join(direct, zfname)
            col = ImageCollection(ID=str(uuid.uuid4()), name="SNAP", downloadable=True, started_at=datetime.utcnow(), nb_images=0, filename=fp)
            self.__session.add(col)
            self.__session.commit()
        return col


    def get_events(self):
        events = self.__session.query(EventQueue).all()
        return events

    def delete(self, obj):
        self.__session.delete(obj)
        self.__session.commit()
    def commit(self):
        self.__session.commit()


def store_img(img_data, img_filename, tar_filename):
    opened = False
    tries = 0
    debug("[store_img] Storing image %s in %s" % (img_filename, tar_filename))
    try:
        z = tarfile.open(tar_filename, 'a')
        opened = True
    except tarfile.ReadError:
        pass

    data  = io.BytesIO(img_data)
    tar_info = tarfile.TarInfo(img_filename)
    tar_info.size = len(img_data)
    tar_info.type = tarfile.REGTYPE
    z.addfile(tar_info, data)
    z.close()
    debug("[store_img] Stored image %s in %s" % (img_filename, tar_filename))


class Grabber(object):
    def __init__(self, camera, camera_nb):
        self.camera = camera
        self.__db = DB(session)
        self.camera_nb = camera_nb
        self.img_nb = 0
        self.shutterspeed = self.camera.exposurespeed
        self.fn_format = "cam{camera_nb}_{timestamp}_{img_nb}.{img_subnb}.jpeg"
        self.old_median = 0 
        self.__current_img_col = self.__db.get_last_image_collection()
        self.__current_snapshot_col = self.__db.get_last_snapshot_collection()

        self.last_image = time.time()
        self.last_preview = time.time()
        debug("Camera %d initialised" % self.camera_nb)

    @property
    def db(self):
        return self.__db


    def store_preview(self):
        debug("Camera %d:  Getting Preview" % self.camera_nb)
        preview =  self.camera.read(resolution=(533,400))
        self.__db.store_preview_img(preview)
        debug("Camera %d:  Preview stored in database!" % self.camera_nb)
        return mk_np_array(preview)

    def __mk_img_data(self, subnb):
        d = {}
        d['img_nb'] = self.img_nb
        d['img_subnb'] = subnb
        d['camera_nb'] = self.camera_nb
        ts = datetime.utcnow() 
        d['timestamp'] = ts.strftime("%Y-%m-%d_%H:%M:%S.%f")
        return d
    
    def __get_median(self, preview):
        img = cv2.imdecode(preview, 0)
        return np.median(img)
        
    def calibrate(self, img):
        self.shutterspeed = self.camera.exposurespeed
        median = self.__get_median(img)
        if (median < 50 or median > 136) and (int(median) != int(self.old_median)):
            old_shutterspeed = self.shutterspeed
            delta = self.shutterspeed * (128 - median) / 256
            self.shutterspeed += delta
            self.old_median = median
            info("Camera %d: new shutterspeed: %d (median brightness: %d)" % (self.camera_nb, self.shutterspeed, median))


    def grab(self, nb_images=1, resolution=None):
        images = []
        subnr = 0
        self.camera.shutterspeed = int(self.shutterspeed)
        debug("Camera %d: Grabbing %d images. resolution = %s" % (self.camera_nb, nb_images, str(resolution)))
        for i in range(nb_images): 
            self.camera.exif('EXIF.UserComment', "Catchcam")
            self.camera.exif('EXIF.ImageUniqueID', uuid.uuid1())
            images.append((self.camera.read(resolution), self.__mk_img_data(subnr)))
            debug("Camera %d: Grabed image %d." % (self.camera_nb, i))
            self.img_nb += 1
            subnr += 1
        return images

    def calibrate_and_preview(self):
        self.camera.shutterspeed = int(self.shutterspeed)
        preview = self.store_preview()
#        self.calibrate(preview)

    def mk_snapshot(self):
        self.__current_snapshot_col = self.__db.get_last_snapshot_collection()
        debug("[mk_snapshot]: making a snapshot")
        max_tar_size = self.__db.max_tar_size 
        images = self.grab(1)
        debug("[mk_snapshot]: got image data.")
        n = self.__current_snapshot_col.nb_images
        self.__current_snapshot_col.nb_images = n + 1
        debug("[mk_snapshot]: number of snapshots taken %d." %n)
        ts = self.__current_snapshot_col.started_at
        fp = self.__current_snapshot_col.filename
        img_dir = os.path.dirname(fp)
        debug("[mk_snapshot]: storing image in %s." % fp)
        if not os.path.exists(img_dir):
            os.makedirs(img_dir)
        for img_data in images:
            img = img_data[0]
            fmtd_name = self.fn_format.format(**img_data[1])
            store_img(img, fmtd_name, fp)
            debug("[mk_snapshot]: stored snapshot")
        self.__db.update_diskspace()

    def grab_and_store(self):
        max_tar_size = self.__db.max_tar_size 
        ts = self.__current_img_col.started_at
        fp = self.__current_img_col.filename
        debug("grab and store has filename: %s" % fp)
        img_dir = os.path.dirname(fp)
        if not os.path.exists(img_dir):
            os.makedirs(img_dir)
        images = self.grab(1)
        for img_data in images:
            img = img_data[0]
            fmtd_name = self.fn_format.format(**img_data[1])
            store_img(img, fmtd_name, fp)
            n = self.__current_img_col.nb_images
            self.__current_img_col.nb_images = n + 1
            self.img_nb +=1

        if self.img_nb >= max_tar_size:
            debug("Tar file contains %d images, starting new tar file" % max_tar_size)
            self.__current_img_col.downloadable = True
            self.__db.commit()
            self.__current_img_col = self.__db.new_image_collection()
            self.__db.commit()
            debug("New set written to %s" %(self.__db.img_directory))
            self.img_nb = 0
        self.__db.commit()
        

    def process_events(self):
        self.__db.update_diskspace()
        events = list(self.__db.get_events())

        if time.time() - self.last_preview > 1:
            debug('Stroring preview')
            self.store_preview()
            self.last_preview = time.time()

        if len(events) > 0:
            debug("Got %d events: " % len(events))
            
            for event in events:
                debug('Got event type: %s' % event.eventName)
                if event.eventName == "START":
                    if self.__db.has_state("STOPPED"):
                        self.__db.set_state("STARTED")
                        self.__current_img_col = self.__db.new_image_collection()
                if event.eventName == "STOP":
                    self.__db.set_state("STOPPED")
                    self.__current_img_col.downloadable = True
                
                if event.eventName == "TAKESNAP":
                    self.mk_snapshot()

            for event in events:
                self.__db.delete(event)

        if self.__db.has_state("STARTED"):
            tdiff = time.time() - self.last_image
            if tdiff  > self.__db.interval:
                debug('Recording image')
                self.grab_and_store()
                self.last_image = time.time()
        
