import cv2
import numpy as np
from sqlalchemy import create_engine, MetaData, Table, String
from sqlalchemy.orm import mapper, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql.expression import or_, and_, not_
engine = create_engine('postgresql://catchcam:CatchCam2017@localhost:5432/catchcam')
Session = sessionmaker(bind=engine)
session = Session()
metadata = MetaData(engine)
Base = declarative_base(metadata=metadata)


class CameraConfig(Base):
    __tablename__ = 'video_cameraconfig'
    __table_args__ = {'autoload': True}

class PreviewImages(Base):
    __tablename__ = 'video_previewimages'
    __table_args__ = {'autoload': True}

class RecordState(Base):
    __tablename__ = 'video_recordstate'
    __table_args__ = {'autoload': True}

class CameraState(Base):
    __tablename__ = 'video_camerastate'
    __table_args__ = {'autoload': True}

preview_obj = session.query(PreviewImages).order_by(PreviewImages.id.desc()).first()

img = np.asarray(preview_obj.image)
cv2.imsave(img,

