#!/usr/bin/env python3
import sys
import select
import time
import camera
import grabber
import logging
from logging import debug, info

logging.basicConfig(format='%(levelname)s %(asctime)s:[Catchcam] %(message)s',filename='/data/catchcam/CatchCam.log',level=logging.DEBUG)

if __name__ == "__main__":
    cam = camera.Camera(1600, 1200, 50)
    info("Camera Opened")
    grab = grabber.Grabber(cam, 0)
    while True:
        grab.process_events()
